import React, {Component} from 'react';
import {Layout, List} from "antd";
import './roomContent.less';
import {getRoomImages} from './paxProducts';
import {selectedMenu} from './actions/room-content/index'
import {connect} from "react-redux";
import {bindActionCreators} from "redux";

const {Header} = Layout;

function mapStateToProps(state) {
  return {};
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({selectedMenu}, dispatch)
}

class RoomContent extends Component {

  componentDidUpdate(prevProps, prevState, snapshot) {
    const roomStyle = this.props.match.params.roomStyle;
    this.props.selectedMenu(roomStyle);
  }

  componentDidMount() {
    const roomStyle = this.props.match.params.roomStyle;
    this.props.selectedMenu(roomStyle);
  }

  render() {
    const roomStyle = this.props.match.params.roomStyle;
    const imageDefault = {headerImage: "", imageList: []}
    const roomImages = getRoomImages()[roomStyle] ? getRoomImages()[roomStyle] : imageDefault
    return (
      <div className="roomImages">
        <Header className="site-layout-sub-header-background">
          <a href={roomImages.headerImageLink}><img className="roomImage" src={roomImages.headerImage}/></a>
        </Header>
        <List
          grid={{
            column: 2,
            gutter: 16
          }}
          dataSource={roomImages.imageList}
          renderItem={item => (
            <List.Item>
              <a href={item.imageLink}><img className="roomContentImage" src={item.imageUrl}/></a>
            </List.Item>
          )}
        />,
      </div>
    );
  }
}

export default connect(
  mapStateToProps, mapDispatchToProps
)(RoomContent)
