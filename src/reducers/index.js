import {combineReducers} from "redux";
import selectedMenuKey from './room-content/index';


const reducers = combineReducers({
    selectedMenuKey
});
export default reducers;