export const initialState = '';

const selectedMenuKey = (state = initialState, action) => {
  switch (action.type) {
    case 'MENU_CHANGED':
        return action.payload;
    default:
      return state;
  }
}

export default selectedMenuKey;
