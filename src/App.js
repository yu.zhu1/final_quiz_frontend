import React, {Component} from 'react';
import './App.less';
import history from "./history";
import {Layout, Menu} from 'antd';
import RoomContent from "./roomContent";
import IcloudIkea from "./icloudIkea";
import 'antd/dist/antd.css';
import {BrowserRouter as Router, Link, Redirect, Route, Switch} from "react-router-dom";
import normalVideo from './video/normalVideo.mp4';
import frontImage from './video/frontImage.jpg';
import {connect} from "react-redux";

const {Sider, Header} = Layout;

function mapStateToProps(state) {
  return {selectedMenu: state.selectedMenuKey};
}

class App extends Component {
  render() {
    return (
      <div className='App'>
        <Router history={history}>
          <Layout>
            <Header className="headImage">
              <video autoPlay muted loop width="100%" src={normalVideo} poster={frontImage}/>
            </Header>

            <Layout>
              <Sider>
                <div className="logo"/>
                <Menu theme="dark" mode="inline" selectedKeys={[this.props.selectedMenu]}>
                  <Menu.Item key="icloudIkea">
                    <Link to="/icloudIkea">
                      <span className="underline">云逛宜家</span>
                    </Link>
                  </Menu.Item>
                  <Menu.Item key="livingRoom">
                    <Link to="/livingRoom"><span className="underline">客厅</span></Link>
                  </Menu.Item>
                  <Menu.Item key="bedroom">
                    <Link to="/bedroom"><span className="underline">卧室</span></Link>
                  </Menu.Item>
                  <Menu.Item key="childRoom">
                    <Link to="/childRoom"><span className="underline">儿童房</span></Link>
                  </Menu.Item>
                  <Menu.Item key="kitchen">
                    <Link to="/kitchen"><span className="underline">厨房</span></Link>
                  </Menu.Item>
                  <Menu.Item key="studyRoom">
                    <Link to="/studyRoom"><span className="underline">书房</span></Link>
                  </Menu.Item>
                </Menu>
              </Sider>

              <Layout className="roomContent">
                <Switch>
                  <Route exact path="/">
                    <Redirect to="/livingRoom"/>
                  </Route>
                  <Route exact path="/icloudIkea" component={IcloudIkea}>
                  </Route>
                  <Route path="/:roomStyle" component={RoomContent}>
                  </Route>
                </Switch>
              </Layout>

            </Layout>
          </Layout>
        </Router>

      </div>
    );

  }


}

export default connect(
  mapStateToProps
)(App);

