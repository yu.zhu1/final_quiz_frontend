export function selectedMenu(menuKey) {
    return {
        type: 'MENU_CHANGED',
        payload: menuKey
    }
 }