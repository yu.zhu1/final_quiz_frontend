import React, {Component} from 'react';
import './icloudIkea.less';
import icloudIkea05 from './images/icloudIkea/icloudIkea05.jpg';
import icloudIkea11 from './images/icloudIkea/icloudIkea11.jpg';
import icloudIkea17 from './images/icloudIkea/icloudIkea17.jpg';
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {selectedMenu} from './actions/room-content/index'

function mapStateToProps(state) {
  return {};
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({selectedMenu}, dispatch)
}

class IcloudIkea extends Component {

  componentDidMount() {
    this.props.selectedMenu('icloudIkea')
  }

  render() {
    return (
      <div className="icloudIkeaImages">
        <a href="https://market.m.taobao.com/app/topping/scenego/web#/tmall?token=A.b.ASUFokrqGTyxpuEZ&floor=%E5%AE%9C%E5%AE%B6%E5%95%86%E5%9C%BA&space=&name=%E5%AE%A2%E5%8E%85%E5%8C%BA">
          <img className="icloudIkeaImage" src={icloudIkea05}/>
        </a>
        <a href="https://market.m.taobao.com/app/topping/scenego/web#/tmall?token=A.b.kSgklysofEclESoi&floor=%E5%AE%9C%E5%AE%B6%E5%95%86%E5%9C%BA&space=&name=%E5%8D%A7%E5%AE%A4%E5%8C%BA">
          <img className="icloudIkeaImage" src={icloudIkea11}/>
        </a>
        <a href="https://market.m.taobao.com/app/topping/scenego/web#/tmall?token=A.b.SAnrTsjjJRTkbYsq&floor=%E5%AE%9C%E5%AE%B6%E5%95%86%E5%9C%BA&space=&name=%E5%84%BF%E7%AB%A5%E5%8C%BA">
          <img className="icloudIkeaImage" src={icloudIkea17}/>
        </a>
      </div>
    );
  }
}


export default connect(
  mapStateToProps, mapDispatchToProps
)(IcloudIkea)
