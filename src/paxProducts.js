import livingRoom07 from './images/livingRoom/livingRoom07.jpg';
import livingRoom15 from './images/livingRoom/livingRoom15.jpg';
import livingRoom17 from './images/livingRoom/livingRoom17.jpg';
import livingRoom25 from './images/livingRoom/livingRoom25.jpg';
import livingRoom27 from './images/livingRoom/livingRoom27.jpg';
import livingRoom35 from './images/livingRoom/livingRoom35.jpg';
import livingRoom37 from './images/livingRoom/livingRoom37.jpg';
import livingRoom45 from './images/livingRoom/livingRoom45.jpg';
import livingRoom47 from './images/livingRoom/livingRoom47.jpg';
import livingRoom55 from './images/livingRoom/livingRoom55.jpg';
import livingRoom57 from './images/livingRoom/livingRoom57.jpg';
import livingRoom65 from './images/livingRoom/livingRoom65.jpg';
import livingRoom67 from './images/livingRoom/livingRoom67.jpg';
import livingRoom75 from './images/livingRoom/livingRoom75.jpg';

import bedroom07 from './images/bedroom/bedroom07.jpg';
import bedroom17 from './images/bedroom/bedroom17.jpg';
import bedroom27 from './images/bedroom/bedroom27.jpg';
import bedroom37 from './images/bedroom/bedroom37.jpg';
import bedroom47 from './images/bedroom/bedroom47.jpg';
import bedroom57 from './images/bedroom/bedroom57.jpg';
import bedroom15 from './images/bedroom/bedroom15.jpg';
import bedroom25 from './images/bedroom/bedroom25.jpg';
import bedroom35 from './images/bedroom/bedroom35.jpg';
import bedroom45 from './images/bedroom/bedroom45.jpg';
import bedroom55 from './images/bedroom/bedroom55.jpg';
import bedroom65 from './images/bedroom/bedroom65.jpg';

import childRoom07 from './images/childRoom/childRoom07.jpg';
import childRoom17 from './images/childRoom/childRoom17.jpg';
import childRoom15 from './images/childRoom/childRoom15.jpg';
import childRoom25 from './images/childRoom/childRoom25.jpg';

import kitchen07 from './images/kitchen/kitchen07.jpg';
import kitchen17 from './images/kitchen/kitchen17.jpg';
import kitchen15 from './images/kitchen/kitchen15.jpg';

import studyRoom07 from './images/studyRoom/studyRoom07.jpg';
import studyRoom17 from './images/studyRoom/studyRoom17.jpg';
import studyRoom15 from './images/studyRoom/studyRoom15.jpg';



export function getRoomImages() {
  return {
    "livingRoom": {
      headerImage: livingRoom07,
      headerImageLink: "https://market.m.taobao.com/app/topping/scenego/web#/tmall?token=A.b.ASUFokrqGTyxpuEZ&floor=%E5%AE%9C%E5%AE%B6%E5%95%86%E5%9C%BA&space=&name=%E5%AE%A2%E5%8E%85%E5%8C%BA",
      imageList: [
        {
          "imageUrl": livingRoom15,
          "imageLink": "https://market.m.taobao.com/app/topping/scenego/web#/tmall?token=A.b.PUzRvfgnMMgwsJpy&floor=%E5%AE%9C%E5%AE%B6%E5%95%86%E5%9C%BA&space=&name=%E5%8C%97%E6%AC%A7%E8%87%AA%E7%84%B6%E9%A3%8E%E5%AE%A2%E5%8E%85%E9%A4%90%E5%8E%85"
        },
        {
          "imageUrl": livingRoom17,
          "imageLink": "https://market.m.taobao.com/app/topping/scenego/web#/tmall?token=A.b.LoTDXfxMTTiAQmNN&floor=%E5%AE%9C%E5%AE%B6%E5%95%86%E5%9C%BA&space=&name=%E5%8C%97%E6%AC%A7%E5%A4%9A%E5%BD%A9%E6%89%8B%E5%81%9A%E5%9D%8A"
        },
        {
          "imageUrl": livingRoom25,
          "imageLink": "https://market.m.taobao.com/app/topping/scenego/web#/tmall?token=A.b.oFruSMxRohQtPBnp&floor=%E5%AE%9C%E5%AE%B6%E5%95%86%E5%9C%BA&space=&name=%E4%BC%A0%E7%BB%9F%E9%AB%98%E9%9B%85%E7%BB%BF%E5%AE%A2%E5%8E%85"
        },
        {
          "imageUrl": livingRoom27,
          "imageLink": "https://market.m.taobao.com/app/topping/scenego/web#/tmall?token=A.b.iRWTfwripWAGArlr&floor=%E5%AE%9C%E5%AE%B6%E5%95%86%E5%9C%BA&space=&name=%E7%8E%B0%E4%BB%A3%E6%9E%81%E7%AE%80%E9%A3%8E%E5%AE%A2%E5%8E%85%E9%98%B3%E5%8F%B0"
        },
        {
          "imageUrl": livingRoom35,
          "imageLink": "https://market.m.taobao.com/app/topping/scenego/web#/tmall?token=A.b.LoLLVFVkBzoxDhSR&floor=%E5%AE%9C%E5%AE%B6%E5%95%86%E5%9C%BA&space=&name=%E5%8C%97%E6%AC%A7%E5%A4%9A%E5%BD%A955%E5%B9%B3%E7%B1%B3%E5%85%AC%E5%AF%93"
        },
        {
          "imageUrl": livingRoom37,
          "imageLink": "https://market.m.taobao.com/app/topping/scenego/web#/tmall?token=A.b.PFBdwmHCthJVjAIX&floor=%E5%AE%9C%E5%AE%B6%E5%95%86%E5%9C%BA&space=&name=%E7%8E%B0%E4%BB%A3%E9%93%81%E9%94%88%E7%BA%A2%E5%AE%A2%E5%8E%85"
        },
        {
          "imageUrl": livingRoom45,
          "imageLink": "https://market.m.taobao.com/app/topping/scenego/web#/tmall?token=A.b.CkJtXtrTUEwFGqEw&floor=%E5%AE%9C%E5%AE%B6%E5%95%86%E5%9C%BA&space=&name=%E4%BC%A0%E7%BB%9F%E6%B5%AA%E6%BC%AB%E7%B2%89%E5%AE%A2%E5%8E%85"
        },
        {
          "imageUrl": livingRoom47,
          "imageLink": "https://market.m.taobao.com/app/topping/scenego/web#/tmall?token=A.b.khDuCODfVUDwVlOO&floor=%E5%AE%9C%E5%AE%B6%E5%95%86%E5%9C%BA&space=&name=%E5%8C%97%E6%AC%A7%E6%B8%90%E5%8F%98%E6%A9%99%E5%AE%A2%E5%8E%85"
        },
        {
          "imageUrl": livingRoom55,
          "imageLink": "https://market.m.taobao.com/app/topping/scenego/web#/tmall?token=A.b.QamvHmcNUSjwavXP&floor=%E5%AE%9C%E5%AE%B6%E5%95%86%E5%9C%BA&space=&name=%E6%96%B0%E4%B8%AD%E5%BC%8F%E5%AE%A2%E5%8E%85%E9%A4%90%E5%8E%85%E4%B9%A6%E6%88%BF"
        },
        {
          "imageUrl": livingRoom57,
          "imageLink": "https://market.m.taobao.com/app/topping/scenego/web#/tmall?token=A.b.yOMtMckNAFfMzZao&floor=%E5%AE%9C%E5%AE%B6%E5%95%86%E5%9C%BA&space=&name=%E7%8E%B0%E4%BB%A3%E6%B8%A9%E9%A6%A8%E5%B0%8F%E5%AE%A2%E5%8E%85"
        },
        {
          "imageUrl": livingRoom65,
          "imageLink": "https://market.m.taobao.com/app/topping/scenego/web#/tmall?token=A.b.mBTSxqGBUEdLouiQ&floor=%E5%AE%9C%E5%AE%B6%E5%95%86%E5%9C%BA&space=&name=%E5%8C%97%E6%AC%A7%E4%BC%A0%E7%BB%9F%E9%A3%8E%E5%AE%A2%E5%8E%85"
        },
        {
          "imageUrl": livingRoom67,
          "imageLink": "https://market.m.taobao.com/app/topping/scenego/web#/tmall?token=A.b.YAFGylyPAzBDwdHk&floor=%E5%AE%9C%E5%AE%B6%E5%95%86%E5%9C%BA&space=&name=%E7%8E%B0%E5%9C%A8%E7%BB%8F%E5%85%B8%E9%A3%8E%E4%B9%A6%E6%88%BF"
        },
        {
          "imageUrl": livingRoom75,
          "imageLink": "https://market.m.taobao.com/app/topping/scenego/web#/tmall?token=A.b.dkUVzhYBNlQAaPbp&floor=%E5%AE%9C%E5%AE%B6%E5%95%86%E5%9C%BA&space=&name=%E5%8C%97%E6%AC%A7%E7%BB%8F%E5%85%B8%E8%93%9D%E5%AE%A2%E5%8E%85"
        }
      ]
    },
    "bedroom": {
      headerImage: bedroom07,
      headerImageLink: "https://market.m.taobao.com/app/topping/scenego/web#/tmall?token=A.b.kSgklysofEclESoi&floor=%E5%AE%9C%E5%AE%B6%E5%95%86%E5%9C%BA&space=&name=%E5%8D%A7%E5%AE%A4%E5%8C%BA",
      imageList: [
        {
          "imageUrl": bedroom15,
          "imageLink": "https://market.m.taobao.com/app/topping/scenego/web#/tmall?token=A.b.JLRvDmaausZvjend&floor=%E5%AE%9C%E5%AE%B6%E5%95%86%E5%9C%BA&space=&name=%E7%8E%B0%E4%BB%A3%E6%B5%AA%E6%BC%AB%E7%B4%AB%E5%8D%A7%E5%AE%A4"
        },
        {
          "imageUrl": bedroom17,
          "imageLink": "https://market.m.taobao.com/app/topping/scenego/web#/tmall?token=A.b.aQknnrrVeJMTLEkf&floor=%E5%AE%9C%E5%AE%B6%E5%95%86%E5%9C%BA&space=&name=%E5%8C%97%E6%AC%A7%E8%87%AA%E7%84%B6%E9%A3%8E%E5%8D%A7%E5%AE%A4"
        },
        {
          "imageUrl": bedroom25,
          "imageLink": "https://market.m.taobao.com/app/topping/scenego/web#/tmall?token=A.b.MtfCTluTEJZUQBGh&floor=%E5%AE%9C%E5%AE%B6%E5%95%86%E5%9C%BA&space=&name=%E8%80%81%E4%B8%8A%E6%B5%B7%E9%A3%8E%E5%8D%A7%E5%AE%A4"
        },
        {
          "imageUrl": bedroom27,
          "imageLink": "https://market.m.taobao.com/app/topping/scenego/web#/tmall?token=A.b.ILKlqEFIpcpYFQrk&floor=%E5%AE%9C%E5%AE%B6%E5%95%86%E5%9C%BA&space=&name=%E4%BC%A0%E7%BB%9F%E7%AE%80%E7%BA%A6%E9%A3%8E%E5%8D%A7%E5%AE%A4"
        },
        {
          "imageUrl": bedroom35,
          "imageLink": "https://market.m.taobao.com/app/topping/scenego/web#/tmall?token=A.b.ggVaJfpziUbZgpca&floor=%E5%AE%9C%E5%AE%B6%E5%95%86%E5%9C%BA&space=&name=%E7%8E%B0%E4%BB%A3%E7%AE%80%E7%BA%A6%E7%9A%AE%E5%BA%8A%E5%8D%A7%E5%AE%A4"
        },
        {
          "imageUrl": bedroom37,
          "imageLink": "https://market.m.taobao.com/app/topping/scenego/web#/tmall?token=A.b.tWdlnXUyvfvtmkZO&floor=%E5%AE%9C%E5%AE%B6%E5%95%86%E5%9C%BA&space=&name=%E8%92%B8%E6%B1%BD%E6%B3%A2%E9%A3%8E%E5%8D%A7%E5%AE%A4%E8%A1%A3%E5%B8%BD%E9%97%B4"
        },
        {
          "imageUrl": bedroom45,
          "imageLink": "https://market.m.taobao.com/app/topping/scenego/web#/tmall?token=A.b.OXQQLghnnTWYRMOq&floor=%E5%AE%9C%E5%AE%B6%E5%95%86%E5%9C%BA&space=&name=%E4%BC%A0%E7%BB%9F%E8%87%AA%E7%84%B6%E9%A3%8E%E5%8D%A7%E5%AE%A4"
        },
        {
          "imageUrl": bedroom47,
          "imageLink": "https://market.m.taobao.com/app/topping/scenego/web#/tmall?token=A.b.eWhKpOMlNSXPNkeM&floor=%E5%AE%9C%E5%AE%B6%E5%95%86%E5%9C%BA&space=&name=%E7%8E%B0%E4%BB%A3%E6%B8%85%E6%96%B0%E7%B2%89%E5%8F%8C%E5%BA%8A%E5%8D%A7%E5%AE%A4"
        },
        {
          "imageUrl": bedroom55,
          "imageLink": "https://market.m.taobao.com/app/topping/scenego/web#/tmall?token=A.b.nsNIWUTHRcbPmEfs&floor=%E5%AE%9C%E5%AE%B6%E5%95%86%E5%9C%BA&space=&name=%E5%8C%97%E6%AC%A7%E4%BC%A0%E7%BB%9F%E9%A3%8E%E6%AC%A1%E5%8D%A7%E5%AE%A4"
        },
        {
          "imageUrl": bedroom57,
          "imageLink": "https://market.m.taobao.com/app/topping/scenego/web#/tmall?token=A.b.tjxBbIzPwvwtrkNV&floor=%E5%AE%9C%E5%AE%B6%E5%95%86%E5%9C%BA&space=&name=%E5%8C%97%E6%AC%A7%E9%A3%8E35%E5%B9%B3%E7%B1%B3%E5%B0%8F%E5%85%AC%E5%AF%93"
        },
        {
          "imageUrl": bedroom65,
          "imageLink": "https://market.m.taobao.com/app/topping/scenego/web#/tmall?token=A.b.nbsPzTFDkjTeZtIO&floor=%E5%AE%9C%E5%AE%B6%E5%95%86%E5%9C%BA&space=&name=%E4%BC%A0%E7%BB%9F%E5%8F%A4%E5%85%B8%E7%BA%A2%E5%8D%A7%E5%AE%A4"
        }
      ]
    },
    "childRoom": {
      headerImage: childRoom07,
      headerImageLink: "https://market.m.taobao.com/app/topping/scenego/web#/tmall?token=A.b.SAnrTsjjJRTkbYsq&floor=%E5%AE%9C%E5%AE%B6%E5%95%86%E5%9C%BA&space=&name=%E5%84%BF%E7%AB%A5%E5%8C%BA",
      imageList: [
        {
          "imageUrl": childRoom15,
          "imageLink": "https://market.m.taobao.com/app/topping/scenego/web#/tmall?token=A.b.nOvZVfkFsKqFWzpW&floor=%E5%AE%9C%E5%AE%B6%E5%95%86%E5%9C%BA&space=&name=%E5%8C%97%E6%AC%A7%E9%99%AA%E4%BC%B4%E5%BC%8F%E5%84%BF%E7%AB%A5%E6%88%BF"
        },
        {
          "imageUrl": childRoom17,
          "imageLink": "https://market.m.taobao.com/app/topping/scenego/web#/tmall?token=A.b.IbXOXVtmcPLvuucX&floor=%E5%AE%9C%E5%AE%B6%E5%95%86%E5%9C%BA&space=&name=%E7%BB%BF%E9%87%8E%E4%BB%99%E8%B8%AA%E5%84%BF%E7%AB%A5%E6%88%BF"
        },
        {
          "imageUrl": childRoom25,
          "imageLink": "https://market.m.taobao.com/app/topping/scenego/web#/tmall?token=A.b.vgPSxwUTQrcDhYst&floor=%E5%AE%9C%E5%AE%B6%E5%95%86%E5%9C%BA&space=&name=%E7%8E%B0%E4%BB%A3%E5%A4%9A%E5%BD%A9%E5%84%BF%E7%AB%A5%E6%88%BF"
        }
      ]
    },
    "kitchen": {
      headerImage: kitchen07,
       headerImageLink: "",
      imageList: [
        {
          "imageUrl": kitchen15,
          "imageLink": "https://market.m.taobao.com/app/topping/scenego/web#/tmall?token=A.b.GNGpusOtiZDFvmKQ&floor=%E5%AE%9C%E5%AE%B6%E5%95%86%E5%9C%BA&space=&name=%E5%8C%97%E6%AC%A7%E4%BC%A0%E7%BB%9F%E9%A3%8E%E9%A4%90%E5%8E%85%E5%8E%A8%E6%88%BF"
        },
        {
          "imageUrl": kitchen17,
          "imageLink": "https://market.m.taobao.com/app/topping/scenego/web#/tmall?token=A.b.YJwmoneGkueyHEWO&floor=%E5%AE%9C%E5%AE%B6%E5%95%86%E5%9C%BA&space=&name=%E7%8E%B0%E4%BB%A3%E7%AE%80%E7%BA%A6%E9%A3%8E%E9%A4%90%E5%8E%85%E5%8E%A8%E6%88%BF"
        }
      ]
    },
    "studyRoom": {
      headerImage: studyRoom07,
      headerImageLink: "https://market.m.taobao.com/app/topping/scenego/web#/tmall?token=A.b.qANcylpsclNHqqHi&floor=%E5%AE%9C%E5%AE%B6%E5%95%86%E5%9C%BA&space=&name=%E5%8A%9E%E5%85%AC%E5%B1%95%E5%8E%85",
      imageList: [
        {
          "imageUrl": studyRoom15,
          "imageLink": "https://market.m.taobao.com/app/topping/scenego/web#/tmall?token=A.b.TsuechIqTQBgdwyt&floor=%E5%AE%9C%E5%AE%B6%E5%95%86%E5%9C%BA&space=&name=%E4%BC%A0%E7%BB%9F%E5%A4%8D%E5%8F%A4%E9%A3%8E%E4%B9%A6%E6%88%BF"
        },
        {
          "imageUrl": studyRoom17,
          "imageLink": "https://market.m.taobao.com/app/topping/scenego/web#/tmall?token=A.b.CIvRIxpFjFSeSaLb&floor=%E5%AE%9C%E5%AE%B6%E5%95%86%E5%9C%BA&space=&name=%E7%8E%B0%E4%BB%A3%E7%BB%8F%E5%85%B8%E8%93%9D%E5%B7%A5%E4%BD%9C%E5%AE%A4"
        }
      ]
    }
  }
}

